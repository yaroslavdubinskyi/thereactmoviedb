import React, { Component } from 'react';
import TMDB_API from '../api/TMDB_API';
import {Helmet} from "react-helmet";

import SerialDetails from '../components/Single/SerialDetails';
import SerialRecomendations from '../components/Single/SerialRecomendations';
import SerialSimilar from '../components/Single/SerialSimilar';
import SerialSeasons from '../components/Single/SerialSeasons';

class SingleTV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serialID: props.match.params.tv_id,
      serialDetails: {
        seasons: [],
        name: 'Name'
      },
      serialRecomendations: [],
      serialSimilar: [],
      genres: []
    }
  }

  componentWillMount() {
    window.scrollTo(0, 0);
    this.getDetails();
    this.getRecomendations();
    this.getSimilars();
    this.getGenres();
  }

  componentWillReceiveProps(newProps) {
    if(newProps === undefined) {
      return false
    }
    if (this.state.serialID !== newProps.match.params.tv_id) {
      const id = newProps.match.params.tv_id;
      this.setState({
        serialID: id
      }, () =>{
        window.scrollTo(0, 0);
        this.getDetails();
        this.getRecomendations();
        this.getSimilars();
      });
    }
  }

  getDetails = () => {
    const serial_id = this.state.serialID;
    TMDB_API.getTVDetails(serial_id, data => {
      this.setState({
        serialDetails: data
      });
    });
  }

  getRecomendations = () => {
    const serial_id = this.state.serialID;
    TMDB_API.getTVRecomendations(serial_id, data => {
      this.setState({
        serialRecomendations: data.slice(0,5)
      });
    });
  }

  getSimilars = () => {
    const serial_id = this.state.serialID;
    TMDB_API.getTVSimilar(serial_id, data => {
      this.setState({
        serialSimilar: data.slice(0,5)
      });
    });
  }

  getGenres = () => {
    TMDB_API.getTVGenres(data => {
      const genres = data.genres;
      this.setState({
        genres
      });
    })
  }

  render() {
    const { serialDetails, serialRecomendations, serialSimilar, genres } = this.state;
    return (
      <div className="SingleTV">
        <Helmet>
          <title>{`${serialDetails.name} - TheReactMovieDB`}</title>
        </Helmet>
        <SerialDetails serialDetails={serialDetails}/>
        <div className="container">
          <SerialSeasons items={serialDetails.seasons} />
          <SerialRecomendations items={serialRecomendations} genres={genres}/>
          <SerialSimilar items={serialSimilar} genres={genres}/>
        </div>
      </div>
    );
  }



}

export default SingleTV;