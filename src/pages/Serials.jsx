import React, { Component } from 'react';
import TMDB_API from "../api/TMDB_API";
import {Helmet} from "react-helmet";

import SearchBar from '../components/SearchBar/SearchBar';
import ElementsGrid from '../components/ElementsGrid';

class Serials extends Component {
  state = {
    serials: [],
    genres: [],
    totalPages: 0,
    currentPage: 1,
    orderBy: 'popularity',
    order: 'desc',
    gridView: 1,
    isFetching: true
  };

  componentWillMount() {
    this.getGenres();
    this.getTVs();
  }

  getTVs = (override = false) => {
    const orderBy = this.state.orderBy;
    const order = this.state.order;
    const currentPage = this.state.currentPage;

    TMDB_API.discoverTV(orderBy, order, currentPage, data => {
      const totalPages = data.total_pages;
      const old_serials = this.state.serials;
      const new_serials = data.results;
      const serials = override ? new_serials : [ ...old_serials, ...new_serials ];
      this.setState({
        serials,
        totalPages,
        isFetching: false
      });
    })
  };

  getGenres = () => {
    TMDB_API.getTVGenres(data => {
      const genres = data.genres;
      this.setState({
        genres
      });
    })
  };

  loadNextPage = () => {
    if(this.state.totalPages > this.state.currentPage){
      const next_page = this.state.currentPage + 1;
      this.setState({ currentPage: next_page}, ()=>{
        this.getTVs();
      });
    }
  };

  changeOrder = (order_by) => {
    if(order_by === this.state.orderBy){
      if(this.state.order === "desc"){
        this.setState({ order: "asc", isFetching: true}, ()=>{
          this.getTVs(true);
        });
      }else{
        this.setState({ order: "desc", isFetching: true}, ()=>{
          this.getTVs(true);
        });
      }
    }else{
      this.setState({ orderBy: order_by, isFetching: true}, ()=>{
        this.getTVs(true);
      });
    }
  };

  render() {
    const { serials, genres, orderBy, order, isFetching } = this.state;
    return (
      <div className="Serials">
        <Helmet>
          <title>TV Shows - TheReactMovieDB</title>
        </Helmet>
        <SearchBar />
        <div className="container">
          <ElementsGrid
            classes="tv-grid"
            items={serials}
            genres={genres}
            loadNextPage={this.loadNextPage}
            orderBy={orderBy}
            order={order}
            changeOrder={this.changeOrder}
            fetching={isFetching}
          />
        </div>
      </div>
    );
  }
}

export default Serials;