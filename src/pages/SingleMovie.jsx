import React, { Component } from 'react';
import TMDB_API from '../api/TMDB_API';
import {Helmet} from "react-helmet";

import MovieDetails from '../components/Single/MovieDetails';
import MovieRecomendations from '../components/Single/MovieRecomendations';
import MovieSimilar from '../components/Single/MovieSimilar';

class SingleMovie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movieID: props.match.params.movie_id,
      movieDetails: {
        name: 'Name'
      },
      movieRecomendations: [],
      movieSimilar: [],
      genres: []
    }
  }

  componentWillMount() {
    window.scrollTo(0, 0);
    this.getDetails();
    this.getRecomendations();
    this.getSimilars();
    this.getGenres();
  }
  componentWillReceiveProps(newProps) {
    if(newProps === undefined) {
      return false
    }

    if (this.state.movieID !== newProps.match.params.movie_id) {
      const id = newProps.match.params.movie_id;
      this.setState({
        movieID: id
      }, () =>{
        window.scrollTo(0, 0);
        this.getDetails();
        this.getRecomendations();
        this.getSimilars();
      });

    }

  }
  getDetails = () => {
    const movie_id = this.state.movieID;
    TMDB_API.getMovieDetails(movie_id, data => {
      this.setState({
        movieDetails: data
      });
    });
  }

  getRecomendations = () => {
    const movie_id = this.state.movieID;
    TMDB_API.getMovieRecomendations(movie_id, data => {
      this.setState({
        movieRecomendations: data.slice(0,5)
      });
    });
  }

  getSimilars = () => {
    const movie_id = this.state.movieID;
    TMDB_API.getMovieSimilar(movie_id, data => {
      this.setState({
        movieSimilar: data.slice(0,5)
      });
    });
  }

  getGenres = () => {
    TMDB_API.getMoviesGenres(data => {
      const genres = data.genres;
      this.setState({
        genres
      });
    })
  }

  render() {
    const { movieDetails, movieRecomendations, movieSimilar, genres } = this.state;
    return (
      <div className="SingleMovie">
        <Helmet>
          <title>{`${movieDetails.title} - TheReactMovieDB`}</title>
        </Helmet>
        <MovieDetails movieDetails={movieDetails}/>
        <div className="container">
          <MovieRecomendations items={movieRecomendations} genres={genres}/>
          <MovieSimilar items={movieSimilar} genres={genres}/>
        </div>
      </div>
    );
  }
}

export default SingleMovie;