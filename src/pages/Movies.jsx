import React, { Component } from 'react';
import TMDB_API from "../api/TMDB_API";
import {Helmet} from "react-helmet";

import SearchBar from '../components/SearchBar/SearchBar';
import ElementsGrid from '../components/ElementsGrid';

class Movies extends Component {
  state = {
    films: [],
    genres: [],
    totalPages: 0,
    currentPage: 1,
    orderBy: 'popularity',
    order: 'desc',
    gridView: 1,
    isFetching: true
  };

  componentWillMount() {
    this.getGenres();
    this.getMovies();
  }

  getMovies = (override = false) => {
    const orderBy = this.state.orderBy;
    const order = this.state.order;
    const currentPage = this.state.currentPage;

    TMDB_API.discoverMovie(orderBy, order, currentPage, data => {
      const totalPages = data.total_pages;
      const old_films = this.state.films;
      const new_films = data.results;
      const films = override ? new_films : [ ...old_films, ...new_films ];
      this.setState({
        films,
        totalPages,
        isFetching: false
      });
    })

  };

  getGenres = () => {
    TMDB_API.getMoviesGenres(data => {
      const genres = data.genres;
      this.setState({
        genres
      });
    })
  };

  loadNextPage = () => {
    if(this.state.totalPages > this.state.currentPage){
      const next_page = this.state.currentPage + 1;
      this.setState({ currentPage: next_page}, ()=>{
        this.getMovies();
      });
    }
  };

  changeOrder = (order_by) => {
    if(order_by === this.state.orderBy){
      if(this.state.order === "desc"){
        this.setState({ order: "asc", isFetching: true}, ()=>{
          this.getMovies(true);
        });
      }else{
        this.setState({ order: "desc", isFetching: true}, ()=>{
          this.getMovies(true);
        });
      }
    }else{
      this.setState({ orderBy: order_by, isFetching: true}, ()=>{
        this.getMovies(true);
      });
    }
  };

  render() {
    const { films, genres, orderBy, order, isFetching } = this.state;
    return (
      <div className="Home">
        <Helmet>
          <title>Movies - TheReactMovieDB</title>
        </Helmet>
        <SearchBar />
        <div className="container">
          <ElementsGrid
            classes="films-grid"
            items={films}
            genres={genres}
            loadNextPage={this.loadNextPage}
            orderBy={orderBy}
            order={order}
            changeOrder={this.changeOrder}
            fetching={isFetching}
          />
        </div>
      </div>
      );
  }
}

export default Movies;