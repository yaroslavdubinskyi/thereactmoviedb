import React, {Component} from 'react';
import TMDB_API from '../api/TMDB_API';
import {Helmet} from "react-helmet";

import SearchBar from '../components/SearchBar/SearchBar';

import SectionLatest from '../components/Home/SectionLatest';

class Home extends Component {
  state = {
    moviesNow: [],
    serialsNow: [],
    genres: [],
  };

  componentWillMount() {
    this.getNowPlayingMovies();
    this.getNowPlayingSerials();
  }

  getNowPlayingMovies = () => {
    TMDB_API.getMoviesNowPlaying(data => {
      const moviesNow = data.results;
      this.setState({
        moviesNow: moviesNow.slice(0,5)
      });
    })
  };

  getNowPlayingSerials = () => {
    TMDB_API.getTVNowPlaying(data => {
      const serialsNow = data.results;
      this.setState({
        serialsNow: serialsNow.slice(0,5)
      });
    })
  };

  render() {
    const { moviesNow, serialsNow} = this.state;
    return (
      <div className="Home">
        <Helmet>
          <title>Home - TheReactMovieDB</title>
        </Helmet>
        <SearchBar />
        <div className="container">
          <SectionLatest movies={moviesNow} serials={serialsNow} />
        </div>
      </div>
    )
  }

}

export default Home;
