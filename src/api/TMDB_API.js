const API_KEY = '24887d21d7f4a96154895b95c9723021';
const BASE_URL = `https://api.themoviedb.org/3`;
class TMDB_API{

  // Movie API calls
  static discoverMovie = (orderBy = 'popularity', order = 'desc', page = 1, callback) => {
    const path = `/discover/movie`;
    const query = `api_key=${API_KEY}&page=${page}&sort_by=${orderBy}.${order}&vote_count.gte=100`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };

  static getMoviesGenres = (callback) => {
    const path = `/genre/movie/list`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };

  static getMovieDetails = (movie_id, callback) => {
    const path = `/movie/${movie_id}`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };

  static getMovieRecomendations = (movie_id, callback) => {
    const path = `/movie/${movie_id}/recommendations`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data.results))
      .catch(error => console.error(error));
  };

  static getMovieSimilar = (movie_id, callback) => {
    const path = `/movie/${movie_id}/similar`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data.results))
      .catch(error => console.error(error));
  };

  // TV API calls
  static discoverTV = (orderBy = 'popularity', order = 'desc', page = 1, callback) => {
    const path = `/discover/tv`;
    const query = `api_key=${API_KEY}&page=${page}&sort_by=${orderBy}.${order}&vote_count.gte=100`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };

  static getTVGenres = (callback) => {
    const path = `/genre/tv/list`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };

  static getTVDetails = (tv_id, callback) => {
    const path = `/tv/${tv_id}`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };

  static getTVRecomendations = (tv_id, callback) => {
    const path = `/tv/${tv_id}/recommendations`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data.results))
      .catch(error => console.error(error));
  };

  static getTVSimilar = (tv_id, callback) => {
    const path = `/tv/${tv_id}/similar`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data.results))
      .catch(error => console.error(error));
  };

  static getMoviesNowPlaying = (callback) => {
    const path = `/movie/now_playing`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };

  static getTVNowPlaying = (callback) => {
    const path = `/tv/on_the_air`;
    const query = `api_key=${API_KEY}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  };


  // Multi Search
  static getMultiSearch = (search_query, callback) => {
    const path = `/search/multi`;
    const query = `api_key=${API_KEY}&query=${search_query}`;
    fetch(`${BASE_URL}${path}?${query}`)
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => console.error(error));
  }



}

export default TMDB_API;