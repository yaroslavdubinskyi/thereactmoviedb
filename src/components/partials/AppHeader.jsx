import React from 'react';
import { Link, NavLink } from 'react-router-dom';

function AppHeader() {
  return(
    <header className="App-header">
      <div className="container">
        <div className="header-inner">
          <div className="logo">
            <Link to={`${process.env.PUBLIC_URL}/`}>
              <h1 className="App-title">The React Movie DB</h1>
            </Link>
          </div>
          <nav className="nav-wrap">
            <ul>
              <li><NavLink exact to={`${process.env.PUBLIC_URL}/`} activeClassName="active">Home</NavLink></li>
              <li><NavLink to={`${process.env.PUBLIC_URL}/movies`} activeClassName="active">Movies</NavLink></li>
              <li><NavLink to={`${process.env.PUBLIC_URL}/tvs`} activeClassName="active">TV</NavLink></li>

            </ul>
          </nav>
        </div>
      </div>
    </header>
  )
}

export default AppHeader;