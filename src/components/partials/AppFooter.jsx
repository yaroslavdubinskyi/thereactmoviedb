import React from 'react';

function AppFooter() {
  return(
    <footer className="App-footer">
      <div className="container">
        <h2>TheReactMovieDB © 2017</h2>
        <h3>Based on <a href="https://www.themoviedb.org" target="_blank" rel="noopener noreferrer">TMDB API</a></h3>
      </div>
    </footer>
  )
}

export default AppFooter;