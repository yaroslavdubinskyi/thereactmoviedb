import React from 'react';
import { Link } from 'react-router-dom';
import RatingBadge from './RatingBadge';
import GenresList from './GenresList';

function GridItem({item, genres}) {

  const id = item.id;
  const title = item.title;
  const name = item.name;
  const poster_url = item.poster_path;
  const poster_path = 'https://image.tmdb.org/t/p/w500' + poster_url;
  const rating = item.vote_average;
  const genres_ids = item.genre_ids;

  return(
    <div className="grid__item" id={id}>
      <Link to={title !== undefined ? `${process.env.PUBLIC_URL}/movie/${id}` : `${process.env.PUBLIC_URL}/tv/${id}`}>
      <img src={poster_path} alt={title}/>
        <div className="desc__wrap">
          <p className="title">{title !== undefined ? title : name}</p>
          <GenresList genres_ids={genres_ids} genres={genres}/>
        </div>
      </Link>
      <RatingBadge rating={rating}/>
    </div>
  )
}

export default GridItem;