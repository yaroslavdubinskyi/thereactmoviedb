import React from 'react';

function GridPagination({loadNextPage}) {
  return(
    <div className="pagination__wrap">
      <button className="button" onClick={loadNextPage}>Load More...</button>
    </div>
  )
}

export default GridPagination;