import React, {Component} from 'react';
import TMDB_API from "../../api/TMDB_API";
import { Redirect } from 'react-router-dom';
import Autosuggest from 'react-autosuggest';

import { debounce as _debounce } from "lodash";

import SearchBarItem from './SearchBarItem';

const getSuggestionValue = suggestion => suggestion.media_type !== 'movie' ? suggestion.name : suggestion.title;

const renderSuggestion = suggestion => (<SearchBarItem item={suggestion} />);

class SearchBar extends Component{

  constructor() {
    super();

    this.state = {
      value: '',
      suggestions: [],
      suggestionLink: null
    };

  }

  loadSuggestions(value) {

    const inputValue = value.trim().toLowerCase();

    const debounceGetMultiSearch = _debounce(TMDB_API.getMultiSearch, 200);

    debounceGetMultiSearch(inputValue, data => {
      const suggestions = data.results.slice(0,10);
      this.setState({
        suggestions
      });
    });

  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.loadSuggestions(value);
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  shouldRenderSuggestions = (value) => {
    return value.trim().length > 1;
  };

  render() {
    const { value, suggestions, suggestionLink } = this.state;

    const inputProps = {
      placeholder: 'Search a movie, tv show or person...',
      value,
      onChange: this.onChange,
      className: 'search-bar-input'
    };

    if (suggestionLink) {
      return <Redirect to={suggestionLink}/>;
    }

    return(
      <div className="search-bar-wrap">
        <div className="container">
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            shouldRenderSuggestions={this.shouldRenderSuggestions}
            inputProps={inputProps}

          />

        </div>
      </div>
    )
  }

}

export default SearchBar;