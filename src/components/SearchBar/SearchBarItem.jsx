import React from 'react';
import { Link } from 'react-router-dom';
import MaterialIcon from 'react-google-material-icons'

function SearchBarItem({item}){

  const id = item.id;
  const itemMediaType = item.media_type;

  let itemTitle = 'no title';
  let itemImgPath = null;

  const itemDesc = item.overview;

  if(itemMediaType !== 'movie'){
    itemTitle = item.name;
  }else{
    itemTitle = item.title;
  }

  if(itemMediaType !== 'person'){
    itemImgPath = item.poster_path !== null ? `https://image.tmdb.org/t/p/w92/${item.poster_path}` : null;
  }else{
    itemImgPath = item.profile_path !== null ? `https://image.tmdb.org/t/p/w92/${item.profile_path}` : null;
  }

  return(
    <Link to={`${process.env.PUBLIC_URL}/${itemMediaType}/${id}`}>
    {itemImgPath !== null && (
      <img className="react-autosuggest__suggestion-image" src={itemImgPath} alt={itemTitle}/>
    )}
    <div className="react-autosuggest__suggestion-info">
      <p className="react-autosuggest__suggestion-title">{itemTitle}</p>
      {itemDesc !== null && (
        <p className="react-autosuggest__suggestion-description">{itemDesc}</p>
      )}
    </div>
    <MaterialIcon icon={itemMediaType} size={48} />

    </Link>
  )
}

export default SearchBarItem;