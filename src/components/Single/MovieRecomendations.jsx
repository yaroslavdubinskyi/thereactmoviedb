import React from 'react';
import SinglePageElementsGrid from './SinglePageElementsGrid';

function MovieRecomendations({items, genres}) {
  return(
    <SinglePageElementsGrid items={items} genres={genres} title="Recomendations" />
  )
}

export default MovieRecomendations;