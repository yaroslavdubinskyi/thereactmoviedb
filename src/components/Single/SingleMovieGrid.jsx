import React from 'react';
import GridItem from '../GridItem';

function SingleMovieGrid({items, genres, title}) {
  const movies_list = items.map((item, index) =>
    <GridItem item={item} key={index} genres={genres}/>
  );
  return(
    <div className="single-movie-grid__wrap">
      <p className="section-title">{title}</p>
      <div className="single-movie-grid">
        {movies_list}
      </div>
    </div>
  )
}

export default SingleMovieGrid;