import React from 'react';
import MaterialIcon from 'react-google-material-icons'


function SerialDetails({serialDetails}) {
  const title = serialDetails.name;
  const overview = serialDetails.overview;
  const vote_average = serialDetails.vote_average;


  const first_air_date = serialDetails.first_air_date;
  const last_air_date = serialDetails.last_air_date;

  const number_of_episodes = serialDetails.number_of_episodes;
  const number_of_seasons = serialDetails.number_of_seasons;


  const posterPath = 'https://image.tmdb.org/t/p/w342/' + serialDetails.poster_path;
  const backdropPath = 'https://image.tmdb.org/t/p/w1280/' + serialDetails.backdrop_path;


  return(
    <div className="single-details__bg">
      <img src={backdropPath} className="bg-img" alt="poster" />
      <div className="container">
        <div className="single-details__wrap">
          <div className="col-left">
            <img src={posterPath} alt={title} className="poster"/>
          </div>
          <div className="col-right">
            <div className="description-wrap">
              <h2>{title}</h2>
              <p className="date"><MaterialIcon icon="date_range"/> {first_air_date} - {last_air_date}</p>
              <p><strong>Rating:</strong> {vote_average}</p>

              <p><strong>Seasons:</strong> {number_of_seasons}</p>
              <p><strong>Episodes:</strong> {number_of_episodes}</p>
              <p>{overview}</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  )
}

export default SerialDetails;