import React from 'react';
import SinglePageElementsGrid from './SinglePageElementsGrid';

function MovieSimilar({items, genres}) {
  return(
    <SinglePageElementsGrid items={items} genres={genres}  title="Similar movies"/>
  )
}

export default MovieSimilar;