import React from 'react';
import SinglePageElementsGrid from './SinglePageElementsGrid';

function SerialSimilar({items, genres}) {
  return(
    <SinglePageElementsGrid items={items} genres={genres}  title="Similar TV shows"/>
  )
}

export default SerialSimilar;