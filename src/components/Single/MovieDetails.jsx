import React from 'react';
import MaterialIcon from 'react-google-material-icons'

function MovieDetails({movieDetails}) {
  const title = movieDetails.title;
  const overview = movieDetails.overview;
  const tagline = movieDetails.tagline;

  const release_date = movieDetails.release_date;
  const posterPath = 'https://image.tmdb.org/t/p/w342/' + movieDetails.poster_path;
  const backdropPath = 'https://image.tmdb.org/t/p/w1280/' + movieDetails.backdrop_path;

  return(
    <div className="single-details__bg">
      <img src={backdropPath} className="bg-img" alt={title} />
      <div className="container">
        <div className="single-details__wrap">
          <div className="col-left">
            <img src={posterPath} alt={title} className="poster"/>
          </div>
          <div className="col-right">
            <div className="description-wrap">
              <h2>{title}</h2>
              <h4><i>{tagline}</i></h4>
              <p className="date"><MaterialIcon icon="date_range"/> {release_date}</p>
              <p>{overview}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MovieDetails;