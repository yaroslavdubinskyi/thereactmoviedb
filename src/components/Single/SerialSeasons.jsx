import React from 'react';

function SerialSeasons(props){

  const seasonsItems = props.items;
  const seasonsList = seasonsItems.map((season, index) =>
    <div className="season-item" key={index}>
      <img src={`https://image.tmdb.org/t/p/w342${season.poster_path}`} alt=""/>
      <div className="desc">
        <p><strong>Date:</strong> {season.air_date}</p>
        <p><strong>Episodes:</strong> {season.episode_count}</p>
      </div>
    </div>
  );

  return(
    <div className="serial-seasons-wrap">
      <p className="section-title">Seasons</p>
      <div className="serial-seasons-grid">
        {seasonsList}
      </div>
    </div>
  )
}

export default SerialSeasons;