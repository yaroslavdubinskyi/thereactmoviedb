import React from 'react';
import SinglePageElementsGrid from './SinglePageElementsGrid';

function SerialRecomendations({items, genres}) {
  return(
    <SinglePageElementsGrid items={items} genres={genres} title="Recomendations" />
  )
}

export default SerialRecomendations;