import React from 'react';

function GridFilters({genres}) {
  const genres_list = genres.map((item, index) =>
    <li key={index}>
      {item.name}
    </li>
  );
  return(
    <div className="grid__filters">
      <ul>
        {genres_list}
      </ul>
    </div>
  )
}

export default GridFilters;