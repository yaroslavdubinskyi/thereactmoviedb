import React from 'react';

function GridSortingBar({changeOrder, orderBy, order}) {
  return(
    <div className="grid-sorting__wrap">
      <ul>
        <li>Sort by:</li>
        <li className={orderBy === 'popularity' ? 'active item' : 'item'} onClick={()=> changeOrder('popularity')}>Popularity</li>
        <li className={orderBy === 'vote_average' ? 'active item' : 'item'} onClick={()=> changeOrder('vote_average')}>Rating</li>
        <li className={orderBy === 'release_date' ? 'active item' : 'item'} onClick={()=> changeOrder('release_date')}>Newest</li>
      </ul>
    </div>
  )
}

export default GridSortingBar;