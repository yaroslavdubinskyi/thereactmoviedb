import React from 'react';

function GenresList({genres_ids, genres}) {

  const genres_list = genres
    .filter(function(item) {
      return genres_ids.indexOf(item.id) !== -1;
    })
    .map(item => item.name)
    .join(', ');
  return(
    <p className="genres">{genres_list}</p>
  )
}

export default GenresList;