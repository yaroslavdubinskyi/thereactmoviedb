import React from 'react';

function RatingBadge({rating}) {
  const rating_height = rating * 10;
  let rating_color = '#ff0000';
  if(rating <= 4){
    rating_color = '#ff0000';
  }else{
    if(rating <= 7){
      rating_color = '#feff14';
    }else{
      rating_color = '#0fff16';
    }
  }
  const badge_style = {
    height: `${rating_height}%`,
    backgroundColor: rating_color
  };
  return(
    <div className="item__rating__wrap">
      <div className="bg-outer" style={badge_style}>

      </div>
      <div className="inner">
        <span>{rating}</span>
      </div>
    </div>
  )
}

export default RatingBadge;