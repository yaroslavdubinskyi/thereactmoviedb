import React from 'react';
import { Link } from 'react-router-dom';

function LatestGridItem({item}) {

  const id = item.id;
  const title = item.title;
  const name = item.name;
  const poster_path = 'https://image.tmdb.org/t/p/w1280/' + item.backdrop_path;

  return(
    <div className="item">
      <Link to={title !== undefined ? `${process.env.PUBLIC_URL}/movie/${id}` : `${process.env.PUBLIC_URL}/tv/${id}`}>
        <img src={poster_path} alt={title}/>
        <div className="desc__wrap">
          <p className="title">{title !== undefined ? title : name}</p>
        </div>
      </Link>
    </div>
  )
}

export default LatestGridItem;