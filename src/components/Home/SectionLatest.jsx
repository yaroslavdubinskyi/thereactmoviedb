import React from 'react';
import LatestGridItem from './LatestGridItem';

function SectionLatest({movies, serials}){
  const moviesList = movies.map((item, index) =>
    <LatestGridItem item={item} key={index}/>
  );
  const serialsList = serials.map((item, index) =>
    <LatestGridItem item={item} key={index}/>
  );
  return(
    <section className="section-latest__wrap">
      <div className="col col-left">
        <p className="section-title">Now on TV</p>
        <div className="grid">
          {serialsList}
        </div>
      </div>
      <div className="col col-right">
        <p className="section-title">Now on Cinemas</p>
        <div className="grid">
          {moviesList}
        </div>
      </div>
    </section>
  )
}

export default SectionLatest;