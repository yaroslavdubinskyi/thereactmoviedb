import React from 'react';
import GridItem from './GridItem';
import GridPagination from './GridPagination';
import GridSortingBar from './GridSortingBar';
import GridFilters from './GridFilters';

function ElementsGrid({classes, items, genres, loadNextPage, orderBy, order, changeOrder, fetching}) {

  const grid_items = items.map((item, index) =>
    <GridItem item={item} key={index} genres={genres}/>
  );

  return(
    <div className="elements__grid__wrap">
      <GridSortingBar orderBy={orderBy} order={order} changeOrder={changeOrder}/>
      <GridFilters genres={genres}/>
      {fetching ? (
        <div className="loading" />
      ) : (
        <div className={'elements__grid ' + classes}>
          {grid_items}
        </div>
      )}
      <GridPagination loadNextPage={loadNextPage}/>
    </div>
  )
}

export default ElementsGrid;