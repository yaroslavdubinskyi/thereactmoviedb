import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import {Helmet} from "react-helmet";

// Styles
import './sass/main.scss';
import './sass/animations.scss';
import './sass/media.scss';

// Partials
import AppHeader from './components/partials/AppHeader';
import AppFooter from './components/partials/AppFooter';

// Pages
import Home from './pages/Home';
import Movies from './pages/Movies';
import Serials from './pages/Serials';
import People from './pages/People';
import SingleMovie from './pages/SingleMovie';
import SingleTV from './pages/SingleTV';

class App extends Component {
  render() {
    const baseUrl = process.env.PUBLIC_URL;
    return (
      <div className="App">
        <Helmet>
          <title>TheReactMovieDB</title>
        </Helmet>
        <Router>
          <div>
            <AppHeader />
            <Route render={({ location }) => (
              <main className="page-content">
                <TransitionGroup>
                  <CSSTransition
                    key={location.key}
                    classNames='fadeUp'
                    timeout={700}
                  >
                    <Switch location={location}>
                      <Route exact path={baseUrl + "/"} component={Home}/>
                      <Route path={baseUrl + "/movies"} component={Movies}/>
                      <Route path={baseUrl + "/tvs"} component={Serials}/>
                      <Route path={baseUrl + "/people"} component={People}/>
                      <Route path={baseUrl + "/movie/:movie_id"} component={SingleMovie}/>
                      <Route path={baseUrl + "/tv/:tv_id"} component={SingleTV}/>

                    </Switch>
                  </CSSTransition>
                </TransitionGroup>
              </main>
            )} />
            <AppFooter />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
